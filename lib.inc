section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rax + rdi], 0
        je .end
        inc rax
        jmp .loop
    
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax

    push rdi
    call string-length
    mov rdx, r
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

    
; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax

    mov rax, 1
    mov rdx, 1
    push rdi
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax

    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax

    push rbx
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 10
    dec rsp

    .loop:
        xor rdx, rdx
        div rbx
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .print_loop

    .print_loop:
        mov rdi, 
        call print_string
        mov rsp, rbp
        pop rbp
        pop rbx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax

    cmp rdi, 0
    jl .negative
    call print_uint
    ret

    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    ret
